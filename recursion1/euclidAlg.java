import java.util.*;

public class App {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); 
        System.out.print("Input the dividend and the divisor respectively: "); // Asks the user for the input values
        try {
            int dividend = sc.nextInt();
            int divisor = sc.nextInt();
            System.out.printf("The GCD of %d and %d is: " + euclidAlg(dividend, divisor), dividend, divisor);
        } catch (InputMismatchException | StackOverflowError e) { // error handling for InputMismatchException and StackOverflowError
            e.printStackTrace();
        }
        sc.close(); // closing resources
    }
    // the euclid method
    public static int euclidAlg(int x, int y) {
        if (x % y == 0) { // If the the MOD of x(dividend) and y(divisor) == 0, we return y as its the GCD
            return y;
        }
        return euclidAlg(y, x%y); // Else we just run the program again, this time y is the dividend and the divisor is x MOD y
    }
}
