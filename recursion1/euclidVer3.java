import java.util.*;

public class App {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); 
        while (true) {
            int firstNum = 1, secNum = 1;
            try {
                System.out.print("Input the firstNum and the secNum respectively: ");
                firstNum = sc.nextInt();
                secNum = sc.nextInt();
            } catch (InputMismatchException | StackOverflowError e) { // error handling for InputMismatchException and StackOverflowError
                System.out.println("Invalid input, please try again");
                sc.next(); continue;
            }
            // if the input of the user is invalid, loop again
            if ((firstNum < 0 || secNum < 0) || (firstNum == 0 && secNum == 0)) {
                System.out.println("Invalid input, please try again");
                continue;
            }
            System.out.printf("The GCD of %d and %d is: " + euclidAlg(firstNum, secNum), firstNum, secNum);
            break; // if nothing went wrong, break out of the loop
        }
        
        sc.close(); // closing resources
    }
    // the euclid method
    public static int euclidAlg(int x, int y) {
        if (x == 0) return y; // if only one of the arguments is 0, return the integer that isnt equal to 0
        else if (y == 0) return x;

        if (x % y == 0) return y; // If the the MOD of x(firstNum) and y(secNum) == 0, we return y as its the GCD

        return euclidAlg(y, x%y); // Else we just run the program again, this time y is the firstNum and the secNum is x MOD y
    }
}
