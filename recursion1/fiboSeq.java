import java.util.*;

public class App {
    private static int maxNumber = 0;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Input starting numbers: ");
        int firstStart = sc.nextInt();
        int secStart = sc.nextInt();

        recur(firstStart, secStart);
        sc.close();
    }

    public static void recur(int first, int second) {
        if (maxNumber < 15) {
            System.out.println(first);
            maxNumber++;
            recur(second, first+second);
        }
        return;
    }
}

