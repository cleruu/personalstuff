import java.util.*;

public class App {
    public static int dividend = 1, divisor = 1;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); 
        try {
            while (true) {
                // Asks the user for the input values
                System.out.print("Input the dividend and the divisor respectively: ");
                dividend = sc.nextInt();
                divisor = sc.nextInt();
                // if the input of the user is invalid, loop again
                if ((dividend < 0 || divisor < 0) || (dividend == 0 && divisor == 0)) {
                    System.out.println("Invalid input, please try again");
                    continue;
                }
                break; // if nothing went wrong, break out of the loop
            }
            System.out.printf("The GCD of %d and %d is: " + euclidAlg(dividend, divisor), dividend, divisor);
        } catch (InputMismatchException | StackOverflowError e) { // error handling for InputMismatchException and StackOverflowError
            e.printStackTrace();
        }
        sc.close(); // closing resources
    }
    // the euclid method
    public static int euclidAlg(int x, int y) {
        if (x == 0) return y; // if only one of the arguments is 0, return the integer that isnt equal to 0
        else if (y == 0) return x;

        if (x % y == 0) return y; // If the the MOD of x(dividend) and y(divisor) == 0, we return y as its the GCD

        return euclidAlg(y, x%y); // Else we just run the program again, this time y is the dividend and the divisor is x MOD y
    }
}
