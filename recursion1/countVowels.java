import java.util.*;

public class App {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Input the string: ");
        String[] a = sc.next().toLowerCase().split("");

        System.out.println("Number of vowels in the string: " + countVow(a, 0, 0));

        sc.close();
    }

    public static int countVow(String[] string, int i, int count){
        while (i != string.length) {
            if (string[i].equals("a") || string[i].equals("e") || string[i].equals("i")|| string[i].equals("o") || string[i].equals("u")) {
                count++; i++;
                countVow(string, i, count);
            }
            else i++;
        }
        return count;
    }
}
