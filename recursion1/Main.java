package recursion;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// Scanner instantiation
		Scanner sc = new Scanner(System.in);
		System.out.println("Input a number: ");
	
		// try and catch for the error handling
		try {
			// Asking the user for the number to be inputed and calling the factorial method
			int a = sc.nextInt();
			System.out.println(factorial(a));
		} catch (StackOverflowError | InputMismatchException e){
			// if an error was found, print the stack trace
			e.printStackTrace();
		}
		// closing resources
		sc.close();
	}
	// the factorial method
	public static int factorial (int x) {
		// if the argument x is not equal to 1, we multiply x to the recursively 
		// called factorial method with x decreased by 1
		if (x != 1) 
			return x * factorial(x - 1);
		// if x is equals to 1, return 1
		return 1;
	}				
}
