import java.util.*;

public class App {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); // Asking the user for number
        System.out.print("Input a Number: ");
        try {
            int x = sc.nextInt();
            System.out.println("The factorial of " + x + " is: " + factorial(x)); // prints out the message + the factorial of the number by calling the method
        } catch (InputMismatchException | StackOverflowError e) { // error handling for InputMismatchException and StackOverflowError
            e.printStackTrace();
        }
        sc.close();// closing resources
    }
    // factorial method
    public static int factorial (int x) {
        if (x == 1) return x; // if x(the argument) == 1, we just return x(1)
        return x * factorial(x-1); // else we multiply the current value of x to the recursively called factorial method with x-1 argument
    }

}
