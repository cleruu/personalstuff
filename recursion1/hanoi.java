import java.util.Scanner;
import java.util.InputMismatchException;

public class Index {

    public static void hanoi(int nDisks, String fromBar, String destBar, String auxBar){
        if (nDisks == 1) {
            System.out.printf("Move disk 1 from bar %s to bar %s\n", fromBar, destBar);
        }
        else {
            // move all disk other than last to buffer
            hanoi(nDisks - 1, fromBar, auxBar, destBar);
            // move last disk to 3rd bar
            System.out.printf("Move disk %d from bar %s to bar %s\n", nDisks, fromBar, destBar);
            // move all but the last disk to 3rd bar 
            hanoi(nDisks - 1, auxBar, destBar, fromBar);
        }
    }

    public static void main(String[] args) {
        // Asking the user for the amount of disks in play
        System.out.print("Enter the number of disks: ");
        Scanner sc = new Scanner(System.in);
        try {
            int nDisks = sc.nextInt();
            // calling the method
            hanoi(nDisks, "A", "C", "B");
        } catch (StackOverflowError | InputMismatchException e) {
            e.printStackTrace();
        }
        // closing resources
        sc.close();
    }
}
