import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        Integer[] first = {1,3};
        Integer[] second = {2};
        Integer[] both = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, both, first.length, second.length); 
        Arrays.sort(both);
        if (both.length % 2 == 1) 
            System.out.println(both[both.length / 2]);
        else   
            System.out.println((double)(both[both.length / 2] + both[(both.length / 2) -1]) / 2);
    }

    public static <T> void printMe (T[] x) {
        for (T t : x) {
            System.out.printf("%s ", t);
        }
    }

    public static void printMe (int[] x) {
        for (int t : x) {
            System.out.printf("%s ", t);
        }
    }
}
