public class Main {

   
    // Method to print all the permutations
    public static void permutation(String s, String l) {
        if (s.length() < 1)
            System.out.println(l + s);

        // Set of the characters of the  permutation
        HashSet<Character> hashSet = new HashSet<Character>();
 
        // Iterating over the string characters
        for (int i = 0; i < s.length(); i++) {
        
           
            // If this permutation has already been
            // created, form the next permutation
            if (hashSet.contains(s.charAt(i)))
                continue;
            else
                // Otherwise, update the set
                hashSet.add(s.charAt(i));
 
            // Create the permutation that contains the ith
            // character and the permutation that does not
            // contain the ith character
            String temp = "";
            if (i < s.length() - 1)
                temp = s.substring(0, i) + s.substring(i + 1);
            else
                temp = s.substring(0, i);
            permutation(temp, l + s.charAt(i));
        }
    }
 
    // Driver method
    public static void main(String[] args) {
        String s = "123";
 
        // Sorting the string
        // using char array
        char[] arr = s.toCharArray();
        Arrays.sort(arr);
        s = new String(arr);
 
        permutation(s, "");
    }
}
