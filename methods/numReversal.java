import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input a number to reverse: ");
        int x  = sc.nextInt();
        System.out.println("original: " + x);

        System.out.println("reversed number: " + numReversal(x));
        sc.close();
    }

    static int numReversal (int x) {
        int reversed = 0;
        while (x != 0) {
            int digit = x % 10;
            reversed = reversed * 10 + digit;
            x /= 10;
        }
        return reversed;
    }
}
