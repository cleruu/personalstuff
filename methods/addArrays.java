import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        String[] first = {"hey", "hello"};
        String[] second = {"yo", "hello"};
        // change data type of "both" for other data types. liines 8-9 are the code that adds them
        String[] both = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, both, first.length, second.length); 
        printMe(both);
    }
    // for printing the combined array
    public static <T> void printMe (T[] x) {
        for (T t : x) {
            System.out.printf("%s ", t);
        }
    }
    // overloaded method for int data type
    public static void printMe (int[] x) {
        for (int t : x) {
            System.out.printf("%s ", t);
        }
    }
}
